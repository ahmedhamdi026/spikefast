import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/Services/api.service';
import { ToastrService } from 'src/app/Services/toastr.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {




  Userform = new FormGroup({

    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),

  })

  constructor(private ApiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr:ToastrService) { }

  ngOnInit(): void {
  }

  onSubmit(formValue: any) {

    if (this.Userform.valid) {

      // this.progressRef.start();


      this.ApiService.login(formValue).subscribe(
        res => {

          let error: any = res.error;


          if (error == 0) {

            // this.notifier.notify("success", "welcome Admin");
            this.toastr.success("Welcome,",res.email);

            let id: any = res.id;
            let email: any = res.email;
            let token: any = res.token;
            let firstName: any = res["firstName"];
            let lastName: any = res["lastName"];
            let statue: any = res["statue"];
            let image: any = res["image"];
            let phone: any = res["phone"];

            localStorage.setItem("id", id);
            localStorage.setItem("email", email);
            localStorage.setItem("token", token);
            localStorage.setItem("firstName", firstName);
            localStorage.setItem("lastName", lastName);
            localStorage.setItem("statue", statue);
            localStorage.setItem("image", image);
            localStorage.setItem("Phone", phone);

            this.router.navigate(['/home']).then(() => {
              window.location.reload();
            });


          } else {
            this.toastr.error("Not correct!",res.email);
          }


        });

    } else {

      // this.notifier.notify("warning", "Please fill out all fields correctly!");
      // this.progressRef.complete();
      this.toastr.info("please fail out all faildes!");

    }




  }


}
