import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../Layouts/header/header.component';
import { FooterComponent } from '../Layouts/footer/footer.component';

// import shared libarary
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from "@angular/common/http";


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule ,FormsModule,ReactiveFormsModule ,HttpClientModule 
  ],exports :[
   
    FormsModule,ReactiveFormsModule ,HttpClientModule 
  ]
})
export class LayoutsModule { }
