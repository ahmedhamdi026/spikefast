import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { NotFoundComponent } from './Components/not-found/not-found.component';


const routes: Routes = [
  {path:"home",component:HomeComponent,pathMatch:"full"},
  {path:'user',loadChildren: () => import('./Auth/auth.module').then(m => m.AuthModule)},
  {path:'spike',loadChildren: () => import('./Components/components.module').then(m => m.ComponentsModule)},
  {path:"",redirectTo:"home",pathMatch:"full"},
  {path:"**",redirectTo:"home",pathMatch:"full"},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
