import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { RouterModule, Routes } from '@angular/router';
import { AnalysisComponent } from './Analysis/analysis.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NewsComponent } from './news/news.component';
import { ProductsComponent } from './products/products.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { TeamMemmberComponent } from './team-memmber/team-memmber.component';
import { TermsComponent } from './terms/terms.component';
import { TrainingComponent } from './training/training.component';
import { WalletComponent } from './wallet/wallet.component';
import { BooksComponent } from './books/books.component';
import { AboutComponent } from './about/about.component';
import { MainNotepadComponent } from './main-notepad/main-notepad.component';
import { EducationalLibraryComponent } from './educational-library/educational-library.component';
import { TradersCommunityComponent } from './traders-community/traders-community.component';
import { TradingCompanyComponent } from './trading-company/trading-company.component';

// ~~~~~~~~~~~~~~~~~~services~~~~~~~~~~~~~~~~
import { AuthGuard } from '../Services/auth-guard.service';
import { ComponentResolverService } from './../Services/component-resolver.service';
import { ResultsEvaluationComponent } from './results-evaluation/results-evaluation.component';


export const CompoRoutes =[
  {path:"about",component:AboutComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"analysis",component:AnalysisComponent,pathMatch:"full",canActivate: [AuthGuard]},
  // {path:"blog",component:AnalysisComponent,pathMatch:"full",canActivate: [AuthGuard],resolve:{Blogs:ComponentResolverService}},

  {path:"book",component:BooksComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"contact",component:ContactUsComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"news",component:NewsComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"products",component:ProductsComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"speaker",component:SpeakersComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"teamMember",component:TeamMemmberComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"terms",component:TermsComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"training",component:TrainingComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"wallet",component:WalletComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"educational",component:EducationalLibraryComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"mainNotepad",component:MainNotepadComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"traders",component:TradersCommunityComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"tradingCompany",component:TradingCompanyComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"subscription",component:SubscriptionsComponent,pathMatch:"full",canActivate: [AuthGuard]},
  {path:"ResultsEvaluation",component:ResultsEvaluationComponent,pathMatch:"full",canActivate: [AuthGuard]},

  

]