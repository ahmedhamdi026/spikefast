import { MainNotepadComponent } from './main-notepad/main-notepad.component';
import { ComponentResolverService } from './../Services/component-resolver.service';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
import { HttpClientModule, HttpClient } from "@angular/common/http";

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CompoRoutes} from './Compo.Routes';

import { AboutComponent } from './about/about.component';
import { AnalysisComponent } from './Analysis/analysis.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NewsComponent } from './news/news.component';
import { ProductsComponent } from './products/products.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { TeamMemmberComponent } from './team-memmber/team-memmber.component';
import { TermsComponent } from './terms/terms.component';
import { TrainingComponent } from './training/training.component';
import { WalletComponent } from './wallet/wallet.component';
import { BooksComponent } from './books/books.component';
import { EducationalLibraryComponent } from './educational-library/educational-library.component';
import { TradersCommunityComponent } from './traders-community/traders-community.component';
import { TradingCompanyComponent } from './trading-company/trading-company.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';

// import shared module
import { LayoutsModule } from '../../app/Layouts/layouts.module';
import { ResultsEvaluationComponent } from './results-evaluation/results-evaluation.component';



@NgModule({
  declarations: [
    
    AboutComponent,
    AnalysisComponent,
    ContactUsComponent,
    NewsComponent,
    ProductsComponent,
    SpeakersComponent,
    TeamMemmberComponent,
    TermsComponent,
    TrainingComponent,
    WalletComponent,
    BooksComponent,
    EducationalLibraryComponent,
    MainNotepadComponent,
    TradersCommunityComponent,
    TradingCompanyComponent,
    SubscriptionsComponent,
    ResultsEvaluationComponent,
    
    
  ],
  providers:[
    ComponentResolverService
  ],
  imports: [
    CommonModule,
    LayoutsModule,
    HttpClientModule,
    TranslateModule.forChild({
      defaultLanguage:'ar',
      loader:{
        provide:TranslateLoader,
        useFactory:(createTranslateLoader),
        deps:[HttpClient]
      }
    }),
    
    RouterModule.forChild(CompoRoutes)
  ]
})
export class ComponentsModule { }

export function createTranslateLoader(http:HttpClient){
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');

}


