import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/Services/api.service';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-main-notepad',
  templateUrl: './main-notepad.component.html',
  styleUrls: ['./main-notepad.component.css']
})
export class MainNotepadComponent implements OnInit {

  Cryptocurrency:any[]|undefined;

  constructor(private apiservice:ApiService) { }

  ngOnInit(): void {

    this.apiservice.GetAllCryptocurrency().subscribe(
      res => {
       this.Cryptocurrency = res["data"];

   });

  }

}
