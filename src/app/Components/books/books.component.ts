import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/Services/api.service';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  Books:any[]|undefined;
  imageHost = environment.imageUrl;

  constructor(private apiservice:ApiService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.apiservice.GetAllBooks().subscribe(res=>
      {
        this.Books=res['data']
      })
  }

}
