import { ToastrService } from './../../Services/toastr.service';
import { ApiService } from 'src/app/Services/api.service';
import { environment } from './../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  id:any;
  email:any;

  
  contactForm = new FormGroup({
    title: new FormControl('', Validators.required),
    mess: new FormControl('', Validators.required),
    phone: new FormControl('',),
    email: new FormControl('',),
    user_Id: new FormControl('',),
    

  })
  constructor(
    public translate:TranslateService,
    private ApiService: ApiService,
    private router: Router,
    private toastr:ToastrService) 
  {
    this.email =  localStorage.getItem('email');
    this.id =  localStorage.getItem('id');
  }

  ngOnInit(): void {
  }
  
  onSend(messageValue: any) {

    if (this.contactForm.valid) {
     
      messageValue["user_Id"] = this.id
      messageValue["email"] = this.email
      this.ApiService.ContactUs(messageValue).subscribe(
        res => { 
          // console.log(messageValue);
          this.toastr.success(res.title,"Sent Successfully");
          this.contactForm.reset();
          // this._snackBar.open('Thank','For Registeration');
          

        });
    } else {
      this.toastr.error('Ops!',"please fail out all feilds");
      // this._snackBar.open('Oops','please fill out all feildes correctly!');
    }

  }

}
