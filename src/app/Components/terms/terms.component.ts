import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/Services/api.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  
  Terms:any[]|undefined;
 

  constructor(private apiservice:ApiService) { }

  ngOnInit(): void {
    this.apiservice.GetAllTerms().subscribe(res=>
      {
        this.Terms=res['data']
      })
  }

}
