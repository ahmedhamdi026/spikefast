import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/Services/api.service';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-educational-library',
  templateUrl: './educational-library.component.html',
  styleUrls: ['./educational-library.component.css']
})
export class EducationalLibraryComponent implements OnInit {

  forexTraining:any[]|undefined; 

  lecturer:any|undefined;


  constructor(private apiservice:ApiService) { }

  ngOnInit(): void {
    this.apiservice.GetAllTrainingCalender().subscribe(
      res => {
       this.forexTraining = res["data"];
       let lecturer=res.data[2][0];

   });
  }

}
